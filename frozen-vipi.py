#https://reinforcementlearning4.fun/2019/06/16/gym-tutorial-frozen-lake/
# USED THIS FOR VI AND PI
import gym, gym.envs, gym.envs.toy_text
import json 
import numpy as np
import hiive.mdptoolbox, hiive.mdptoolbox.example, hiive.mdptoolbox.mdp, hiive.visualization
import hiive.mdptoolbox as mdptoolbox

# Settings
f = open("work22.csv","w+")
f.write("State,Action,Reward,Error,Time,Alpha,Epsilon,Gamma,Max V,Mean V,Iteration\n")
np.set_printoptions(suppress=True)
ACTIONS = 4
NUMBER_OF_RUNS = 1
SLIPPERY = [True]
NUMBER_OF_TESTS = 1000 #10000?
TEST_STEPS = 1000 #1000?
#DISCOUNT = 0.99
VERBOSE = True
MAP_SIZES = [16]
SHOW_STATS = False
WORLD = "FrozenLake-v0"

DISCOUNTS = [0.99]

# Set to true if you want to perform that MDP
USE_Q = False
USE_VI = True
USE_PI = True

#VI AND PI SETTINGS - Only applies if USE_VI or USE_PI are true
VP_ITERATIONS = [10000]

#Q Learning Settings ONLY APPLIES IF USE_Q = True
Q_DISCOUNT = 0.99
Q_ITERATIONS = [20000]
ALPHA=.1 #- .1 Learning Rate - How much to remember.  0 remembers nothing??
ALPHA_DECAY=0.99 # .99
ALPHA_MIN=0.1 # .001
EPSILON=1 # 1 Exploration - where 1 is the highest and is completely random
EPSILON_DECAY=0.9995 # .99
EPSILON_MIN=0.0001 # .1
USE_CALLBACK = False
Q_TEST_STEPS = 1000

def callback(s, a, s_new):
    print("%d,%d,%d" % (s,a,s_new))

def convertStateToGrid(state,width):
    if state == 0:
        return 0,0
    return int(state/width), state%width


def testPolicy(policy,maps,slippery,width):
    env = gym.make("FrozenLake-v0", desc=maps, is_slippery=slippery)
    #env = gym.make(WORLD)
    env.reset()
    #env.render()

    state = 0
    action = policy[state]
    done = False
    visits[0][0]+1

    while done == False:
        new_state, reward, done, info = env.step(action)
        x,y = convertStateToGrid(new_state,width)
        visits[x][y] = visits[x][y] + 1
        if done:
            return reward
        action = policy[new_state]
    
    return 0

print ("MapSize,Slippery,Learner,Time(secs),Rewards,Avg Reward,Iterations,PolicyIter,Discount,Alpha,AlphaDecay,AlphaMin,Epsilon,EpsilonDecay,EpsilonMin")

for run in range(0,NUMBER_OF_RUNS):
    for slippery in SLIPPERY:
        for mapsize in MAP_SIZES:
            gridsize = mapsize * mapsize
            visits=np.zeros((mapsize,mapsize))
            v=np.zeros((gridsize))
            maps = gym.envs.toy_text.frozen_lake.generate_random_map(mapsize)
            #env = gym.make(WORLD)
            env = gym.make("FrozenLake-v0", desc=maps, is_slippery=slippery)
            env.reset()

            # Convert from openAI Gym to mdptoolbox
            P=np.zeros((ACTIONS,gridsize,gridsize))
            R=np.zeros((gridsize,ACTIONS))
            for state in range(0,gridsize):
                for action in range(0,ACTIONS):
                    for probability, new_state, reward, finished in env.P[state][action]:
                        #print(probability, state, action, new_state, reward, finished)
                        P[action, state, new_state] += probability
                        R[new_state, action] += reward
            if USE_PI or USE_VI:
                for iterations in VP_ITERATIONS:
                    for discount in DISCOUNTS:
                        if USE_PI:
                            pi = mdptoolbox.mdp.PolicyIteration(P,R,discount,max_iter=iterations)
                            pi.verbose = VERBOSE
                            pi.run()
                            print (pi.policy)
                            totalReward = 0
                            if SHOW_STATS:
                                print(pi.run_stats)
                            for run in range(1,NUMBER_OF_TESTS+1):
                                totalReward += testPolicy(pi.policy,maps,slippery,mapsize)
                                if (run % TEST_STEPS) == 0:
                                    print("%d,%r,PI,%.4f,%d,%d,%.3f,%d,%d,%.2f" % (mapsize,slippery,pi.time,totalReward,(run),totalReward/(run),iterations,pi.iter,discount))

                        if USE_VI:
                            vi = mdptoolbox.mdp.ValueIteration(P,R,discount,max_iter=iterations)
                            vi.verbose = VERBOSE
                            vi.run()
                            print (vi.policy)
                            totalReward = 0
                            if SHOW_STATS:
                                print(vi.run_stats)
                            for run in range(1,NUMBER_OF_TESTS+1):
                                totalReward += testPolicy(vi.policy,maps,slippery,mapsize)
                                if (run % TEST_STEPS) == 0:
                                    print("%d,%r,VI,%.4f,%d,%d,%.3f,%d,%d,%.2f" % (mapsize,slippery,vi.time,totalReward,(run),totalReward/(run),iterations,vi.iter,discount))
                
            if USE_Q:
                q_visits=np.zeros((gridsize))
                total = 0
                tr = 0
                for iterations in Q_ITERATIONS:
                    q = mdptoolbox.mdp.QLearning(P,R,Q_DISCOUNT,run_stat_frequency=1,n_iter=iterations,alpha=ALPHA,alpha_decay=ALPHA_DECAY,alpha_min=ALPHA_MIN,epsilon=EPSILON,epsilon_decay=EPSILON_DECAY,epsilon_min=EPSILON_MIN)
                    if USE_CALLBACK:
                        q.iter_callback = callback
                    q.verbose = VERBOSE
                    q.run()
                    totalReward = 0
                    if SHOW_STATS:
                        #print(q.run_stats)
                        for stat in q.run_stats:
                            total = total+1
                            #print(total)
                            f.writelines("%d,%d,%.1f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%d\n" %(stat.get("State"),stat.get("Action"),stat.get("Reward"),stat.get("Error"), stat.get("Time"),stat.get("Alpha"), stat.get("Epsilon"), stat.get("Gamma"), stat.get("Max V"), stat.get("Mean V"), stat.get("Iteration")))
                            q_visits[stat.get('State')] = q_visits[stat.get('State')] + 1
                            totalReward += stat.get('Reward')
                            if (stat.get('Iteration') % Q_TEST_STEPS) == 0:
                                print(stat.get('Iteration'),totalReward,totalReward/stat.get('Iteration'))
                        print("Q VISITS")
                        print(q_visits)
                        
                        #print(total)
                    #{'State': 10, 'Action': 0, 'Reward': 0.0, 'Error': 0.0, 'Time': 0.6354219913482666, 'Alpha': 0.1, 'Epsilon': 0.01, 'Gamma': 0.99, 'Max V': 46.49059261059508, 'Mean V': 2.9056620381621925, 'Iteration': 10000}
                    totalReward = 0
                    for run in range(1,NUMBER_OF_TESTS+1):
                        totalReward += testPolicy(q.policy,maps,slippery,mapsize)
                        if (run % TEST_STEPS) == 0:
                            print("%d,%r,Q,%.4f,%d,%d,%.3f,%d,%d,%.2f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f" % (mapsize,slippery,q.time,totalReward,(run),totalReward/(run),iterations,iterations,Q_DISCOUNT,ALPHA,ALPHA_DECAY,ALPHA_MIN,EPSILON,EPSILON_DECAY,EPSILON_MIN))
                            
                        
f.close()
                    