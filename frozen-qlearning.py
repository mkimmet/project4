#https://deeplizard.com/learn/video/QK_PP_2KgGE
#This is the q learning I'm using for frozen lake

import numpy as np
import gym, gym.envs, gym.envs.toy_text
import random
import time
from IPython.display import clear_output
import time
start_time = time.time()
mapsize = 8
slippery = True
env = gym.make("FrozenLake8x8-v0")
#maps = gym.envs.toy_text.frozen_lake.generate_random_map(mapsize)
#env = gym.make("FrozenLake-v0", desc=maps, is_slippery=slippery)
np.set_printoptions(suppress=True)
q_visits=np.zeros((mapsize*mapsize))

action_space_size = env.action_space.n
state_space_size = env.observation_space.n
print(state_space_size)

q_table = np.zeros((state_space_size, action_space_size))
print("exploration_rate,state,action,new_state,reward,done,q_table[state,action]")

num_episodes = 100000
max_steps_per_episode = 500 #100

learning_rate = 0.1# .1
discount_rate = 0.9999 #0.99

exploration_rate = 1 #epsilon 1 
max_exploration_rate = 1 # 1
min_exploration_rate = .0001 # 0.0001
exploration_decay_rate = 0.00015 # 0.001

rewards_all_episodes = []

for episode in range(num_episodes):
    state = env.reset()
    done = False
    rewards_current_episode = 0
    print("Episode: ", episode)
    for step in range(max_steps_per_episode): 
        exploration_rate_threshold = random.uniform(0, 1)
        if exploration_rate_threshold > exploration_rate:
            action = np.argmax(q_table[state,:]) 
        else:
            action = env.action_space.sample()
        new_state, reward, done, info = env.step(action)
        q_table[state, action] = q_table[state, action] * (1 - learning_rate) + learning_rate * (reward + discount_rate * np.max(q_table[new_state, :]))
        
        prev_state = state
        state = new_state     
        rewards_current_episode += reward
        q_visits[state] = q_visits[state]+1

        if done == True:
            print(exploration_rate,prev_state,action,new_state,reward,done,q_table[state,action])
            print ("Steps: ", step)
            break

    exploration_rate = min_exploration_rate + (max_exploration_rate - min_exploration_rate) * np.exp(-exploration_decay_rate*episode)

    rewards_all_episodes.append(rewards_current_episode)

rewards_per_thosand_episodes = np.split(np.array(rewards_all_episodes),num_episodes/1000)
count = 1000

print("\n\n**********Q**********\n")
print(q_table)

print("**********Average reward*********\n")
for r in rewards_per_thosand_episodes:
    print(count, ": ", str(sum(r/1000)))
    count += 1000

end_time = time.time()
print("seconds: ",end_time-start_time)

print("q state vists")
print(q_visits)