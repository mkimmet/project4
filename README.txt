#Setup and Environment#

I am using Python with the following libraries that you will need to install:
- openAI Gym
- numpy
- json
- pymdptoolbox
- Hiive's fork of mdptoolbox

##Files and Data##
My data and python files can be found at this repo:
https://bitbucket.org/mkimmet/project4/

## How To Run Analysis##
I have 3 Python files that I used to generate my analysis, 2 for the Frozen Lake problem,
and 1 for the Forest Problem.

##Frozen Lake##

*frozen-vipi.py*
This is the file that generates the VI and PI data/analysis, I did not use it for Q
learning and so the Q learning settings can be ignored.  It has many settings towards 
the top of the file which allow you to control what it will run and output.  Here is a
description of the settings:

NUMBER_OF_RUNS = 1 => Number of times to run the entire analysis
SLIPPERY = [True] => Array or booleans [True, False] would test both slippery and not
NUMBER_OF_TESTS = 1000 => Number of tests to run to get the average reward or your policy
TEST_STEPS = 1000 => number of tests to run before outputing the average reward data
VERBOSE = True => Whether to output hiive's mdptoolbox verbosity during a run
MAP_SIZES = [16] => the size of the map (16 for 4x4 and 64 for 8x8)
SHOW_STATS = False => if true it will show the stats from hiive's mdptoolbox for a policy
WORLD = "FrozenLake-v0" => World name of the frozenlake to run FrozenLake8x8-v0 for the 8x8
DISCOUNTS = [0.99] => Array of discounts to try
VP_ITERATIONS = [10000] => Array of iterations to try for VP and PI

So after you make the desired changes, you then run the file `python frozen-vipi.py` to
get the infor outputted to the screen.  I often would pipe it into a file.

*frozen-qlearning.py*
This is the file that I used for running my Frozen Lake Q Learning analysis.
Similar to the previous file, there are settings towards the top to control
the hyperparameters and settings:

num_episodes = 100000 => Number of iteration to run
max_steps_per_episode = 500 => Maximum number of steps to allow in a given iteration

learning_rate = 0.1 => Alpha Learning Rate
discount_rate = 0.9999 => Discount Rate

exploration_rate = 1 => Epsilon 
max_exploration_rate = 1 => Max Epsilon
min_exploration_rate = .0001 => Min Epsilon
exploration_decay_rate = 0.00015 => Epsilon Decay Rate

## Forest Managment Problem ##

For the Forest Management problem I used only one file:

*forest-multi.py*

This is similar to the other files and you can control the settings with
the variables towards the top of the file.  These are explained here:

NUMBER_OF_RUNS = 1 => Number of times to run the analysis
NUMBER_OF_TESTS = 100 => Number of times to run the tests to get average reward
TEST_YEARS = 1000 => the number of state changes allowed in a run
VERBOSE = True => Output the mdptoolbox verbose details
STATES = [16] => Array of the number of states, if you are running more than one set of states
REWARDS_PROBS = 1 => Number of different sets of rewards and probabilitis you'd like to run
CUT_WAIT_REWARDS = [5] => array of the final cut rewards to try, if you add a number here be sure to increase REWARDS_PROBS by 1 and also add a number to the next two arrays
WAIT_REWARDS = [30] => array of the final wait rewards to try
FIRE_PROBS= [0.01] => array of the fire probabilities to try
SHOW_STATS = True => show the stats from hiive's mdptoolbox

DISCOUNTS = [0.9] => Discount's to try

USE_Q = False => Whether to do the tests for Q Learning
USE_VI = True => Whether to do the tests for VI
USE_PI = True => Whether to do the tests for PI

VP_ITERATIONS = [2000] => VI and PI Max iterations to try

#Q Learning Settings ONLY APPLIES IF USE_Q = True
Q_DISCOUNT = 0.9  #Q Learning Discount
Q_ITERATIONS = [500000] => Q learning number of iterations to run.
ALPHA=.1 
ALPHA_DECAY=0.99 # .99
ALPHA_MIN=0.1 # .001
EPSILON=1 # 
EPSILON_DECAY=0.9999999 # .99
EPSILON_MIN=0.0001 # .1
USE_CALLBACK = False => whether or not to use the callback
Q_TEST_STEPS = 1000


For all the files above, I manipulated the settings to run what I wanted
and then often outputted it to a file.  Then I would take them into a
Google doc to make graphs and do analysis.

You can ignore all the other files in this project, as those were dumps
of runs that I performed, some of which were tests.





## NOTES FOR MARK KIMMET ABOUT MY LOCAL INSTALL TAs CAN PLEASE IGNORE##

**Activate the Virtual Environment for Python**
source ml-class/bin/activate

This will make it so you are running in python 3 environment which is setup with scikit learn

**VSCODE**
I changed the VSCode settings.json to point to the python3 ml-class venv so linting would work:


