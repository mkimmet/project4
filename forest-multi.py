# USED THIS FOR VI, PI, and Q LEARNING FOR FOREST
import gym, gym.envs, gym.envs.toy_text
import json 
import numpy as np
import hiive.mdptoolbox, hiive.mdptoolbox.example, hiive.mdptoolbox.mdp, hiive.visualization
import hiive.mdptoolbox as mdptoolbox
import random

# Settings
f = open("forest-output-3.csv","w+")
f.write("State,Action,Reward,Error,Time,Alpha,Epsilon,Gamma,Max V,Mean V,Iteration\n")
np.set_printoptions(suppress=True)
ACTIONS = 2
NUMBER_OF_RUNS = 1
NUMBER_OF_TESTS = 100 #10000?
TEST_YEARS = 1000 #1000?
VERBOSE = True
STATES = [16] #4
REWARDS_PROBS = 1
CUT_WAIT_REWARDS = [5] #3
WAIT_REWARDS = [30] #4
FIRE_PROBS= [0.01] # 0.1
SHOW_STATS = True

DISCOUNTS = [0.9]

# Set to true if you want to perform that MDP
USE_Q = False
USE_VI = True
USE_PI = True

#VI AND PI SETTINGS - Only applies if USE_VI or USE_PI are true
VP_ITERATIONS = [2000]

#Q Learning Settings ONLY APPLIES IF USE_Q = True
Q_DISCOUNT = 0.9  #9999999999
Q_ITERATIONS = [500000]
ALPHA=.1 #- .1 Learning Rate - How much to remember.  0 remembers nothing??
ALPHA_DECAY=0.99 # .99
ALPHA_MIN=0.1 # .001
EPSILON=1 # 1 Exploration - where 1 is the highest and is completely random
EPSILON_DECAY=0.9999999 # .99
EPSILON_MIN=0.0001 # .1
USE_CALLBACK = False
Q_TEST_STEPS = 1000

def callback(s, a, s_new):
    print("%d,%d,%d" % (s,a,s_new))

def convertStateToGrid(state,width):
    if state == 0:
        return 0,0
    return int(state/width), state%width

def convertQtoPolicy(Q):
    p = []
    for q in Q:
        if q[0] >= q[1]:
            p.append(0)
        else:
            p.append(1)
    return p


def test_policy(policy, states, wait_reward, cut_reward, prob, iterations, discount):
    totalReward = 0
    state = 0
    totalYears = 0
    itermediateCutReward = 1
    while totalYears < iterations:
        rando = random.uniform(0, 1)
        #print(policy[state])
        #print(rando)
        #FIRE start over
        if rando < prob:
            state = 0
        elif policy[state] == 0:
            #do Wait
            if state == states-1:
                #wait in last state get reward and start over
                totalReward = totalReward + wait_reward
                state = 0
            else:
                #no reward go to next state
                state = state + 1
        elif policy[state] == 1:
            #do Cut
            if state == states-1:
                #get cut reward at end and start forest at beginning
                totalReward = totalReward + cut_reward
            else:
                #get cut reward for intermediate cut and start over
                totalReward = totalReward + itermediateCutReward
            state = 0
        #use discount to update the rewards and update the year
        wait_reward = wait_reward * discount
        cut_reward = cut_reward * discount
        itermediateCutReward = itermediateCutReward * discount
        totalYears = totalYears + 1
    return totalReward

print ("MapSize,Slippery,Learner,Time(secs),Rewards,Avg Reward,Iterations,PolicyIter,Discount,Alpha,AlphaDecay,AlphaMin,Epsilon,EpsilonDecay,EpsilonMin")

for run in range(0,NUMBER_OF_RUNS):
    for reward_prob_num in range(0,REWARDS_PROBS):
        for states in STATES:
            P, R = mdptoolbox.example.forest(S=states,r1=WAIT_REWARDS[reward_prob_num],r2=CUT_WAIT_REWARDS[reward_prob_num],p=FIRE_PROBS[reward_prob_num])
            visits=np.zeros((states))
            v=np.zeros((states))

            # TRY TO FIND THE OPTIMAL POLICY
            cut_policy=np.zeros((states))
            cut_last_policy=np.zeros((states))
            wait_policy=np.zeros((states))
            cut_policy[1] = 1
            cut_last_policy[len(cut_last_policy)-1] = 1
            totalReward = 0
            for test in range(0,NUMBER_OF_TESTS):
                totalReward = totalReward + test_policy(cut_policy, states, WAIT_REWARDS[reward_prob_num], CUT_WAIT_REWARDS[reward_prob_num], FIRE_PROBS[reward_prob_num], TEST_YEARS, Q_DISCOUNT)
            print("Cut Policy Total Reward: ", totalReward)
            print("Cut Policy Avg Reward: ", totalReward/NUMBER_OF_TESTS)
            totalReward = 0
            for test in range(0,NUMBER_OF_TESTS):
                totalReward = totalReward + test_policy(cut_last_policy, states, WAIT_REWARDS[reward_prob_num], CUT_WAIT_REWARDS[reward_prob_num], FIRE_PROBS[reward_prob_num], TEST_YEARS, Q_DISCOUNT)
            print("Cut Last Policy Total Reward: ", totalReward)
            print("Cut Last Policy Avg Reward: ", totalReward/NUMBER_OF_TESTS)
            totalReward = 0
            for test in range(0,NUMBER_OF_TESTS):
                totalReward = totalReward + test_policy(wait_policy, states, WAIT_REWARDS[reward_prob_num], CUT_WAIT_REWARDS[reward_prob_num], FIRE_PROBS[reward_prob_num], TEST_YEARS, Q_DISCOUNT)
            print("Wait Policy Total Reward: ", totalReward)
            print("Wait Policy Avg Reward: ", totalReward/NUMBER_OF_TESTS)

            if USE_PI or USE_VI:
                for iterations in VP_ITERATIONS:
                    for discount in DISCOUNTS:
                        if USE_PI:
                            pi = mdptoolbox.mdp.PolicyIteration(P,R,discount,max_iter=iterations)
                            pi.verbose = VERBOSE
                            pi.run()
                            print (pi.policy)
                            totalReward = 0
                            if SHOW_STATS:
                                print(pi.run_stats)
                            for test in range(0,NUMBER_OF_TESTS):
                                totalReward = totalReward + test_policy(pi.policy, states, WAIT_REWARDS[reward_prob_num], CUT_WAIT_REWARDS[reward_prob_num], FIRE_PROBS[reward_prob_num], TEST_YEARS, discount)
                            print("PI Total Reward: ", totalReward)
                            print("PI Avg Reward: ", totalReward/NUMBER_OF_TESTS)
                            print("PI Time: ", pi.time)

                        if USE_VI:
                            vi = mdptoolbox.mdp.ValueIteration(P,R,discount,max_iter=iterations)
                            vi.verbose = VERBOSE
                            vi.run()
                            print (vi.policy)
                            totalReward = 0
                            if SHOW_STATS:
                                print(vi.run_stats)
                            for test in range(0,NUMBER_OF_TESTS):
                                totalReward = totalReward + test_policy(vi.policy, states, WAIT_REWARDS[reward_prob_num], CUT_WAIT_REWARDS[reward_prob_num], FIRE_PROBS[reward_prob_num], TEST_YEARS, discount)
                            print("VI Total Reward: ", totalReward)
                            print("VI Avg Reward: ", totalReward/NUMBER_OF_TESTS)
                            print("VI Time: ", vi.time)
                
            if USE_Q:
                q_visits=np.zeros((states))
                total = 0
                tr = 0
                for iterations in Q_ITERATIONS:
                    q = mdptoolbox.mdp.QLearning(P,R,Q_DISCOUNT,run_stat_frequency=1,n_iter=iterations,alpha=ALPHA,alpha_decay=ALPHA_DECAY,alpha_min=ALPHA_MIN,epsilon=EPSILON,epsilon_decay=EPSILON_DECAY,epsilon_min=EPSILON_MIN)
                    if USE_CALLBACK:
                        q.iter_callback = callback
                    q.verbose = VERBOSE
                    q.run()
                    print (q.Q)
                    totalReward = 0
                    if SHOW_STATS:
                        print(q.run_stats)
                        for stat in q.run_stats:
                            total = total+1
                            #print(total)
                            f.writelines("%d,%d,%.1f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%.4f,%d\n" %(stat.get("State"),stat.get("Action"),stat.get("Reward"),stat.get("Error"), stat.get("Time"),stat.get("Alpha"), stat.get("Epsilon"), stat.get("Gamma"), stat.get("Max V"), stat.get("Mean V"), stat.get("Iteration")))
                            q_visits[stat.get('State')] = q_visits[stat.get('State')] + 1
                            totalReward += stat.get('Reward')
                            if (stat.get('Iteration') % Q_TEST_STEPS) == 0:
                                print(stat.get('Iteration'),totalReward,totalReward/stat.get('Iteration'))
                        print("Q VISITS")
                        print(q_visits)
                        
                        #print(total)
                    #{'State': 10, 'Action': 0, 'Reward': 0.0, 'Error': 0.0, 'Time': 0.6354219913482666, 'Alpha': 0.1, 'Epsilon': 0.01, 'Gamma': 0.99, 'Max V': 46.49059261059508, 'Mean V': 2.9056620381621925, 'Iteration': 10000}
                    totalReward = 0
                    q_policy = convertQtoPolicy(q.Q)
                    print(q_policy)
                    for test in range(0,NUMBER_OF_TESTS):
                        totalReward = totalReward + test_policy(q_policy, states, WAIT_REWARDS[reward_prob_num], CUT_WAIT_REWARDS[reward_prob_num], FIRE_PROBS[reward_prob_num], TEST_YEARS, Q_DISCOUNT)
                    print("Q Total Reward: ", totalReward)
                    print("Q Avg Reward: ", totalReward/NUMBER_OF_TESTS)
                    print("Q Time: ", q.time)
                            
                        
f.close()
                    